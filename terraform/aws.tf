provider "aws" {
  region = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

data "aws_route53_zone" "zone" {
  name = var.aws_route_53_zone_name
}

locals {
  ips = digitalocean_droplet.web.*.ipv4_address
}

resource "aws_route53_record" "dns" {
  count = var.counts
  zone_id = data.aws_route53_zone.zone.zone_id
  name = "${var.personal_email}.${count.index}.${data.aws_route53_zone.zone.name}"
  type = "A"
  ttl = "300"
  records = [element(local.ips, count.index)]
}
