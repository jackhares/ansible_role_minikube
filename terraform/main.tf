provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "MY_SSH_PUB_KEY" {
  name       = var.digitalocean_ssh_key_personal_key
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "digitalocean_tag" "module" {
  name = var.digitalocean_tag_module
}

resource "digitalocean_tag" "email" {
  name = var.personal_email
}

resource "random_string" "droplet_user_root_password" {
  length    = 12
  min_upper = 6
  min_lower = 6
}

locals {
  passwords = random_string.droplet_user_root_password.result
}

output "passwords" {
  value = random_string.droplet_user_root_password.*.result
}

resource "digitalocean_droplet" "web" {
  count    = var.counts
  image    = var.droplet_image_name
  name     = replace("${var.personal_email}${count.index}", "_", "")
  region   = var.droplet_region
  size     = var.droplet_configuration
  ssh_keys = [digitalocean_ssh_key.MY_SSH_PUB_KEY.fingerprint]
  tags     = [digitalocean_tag.module.id, digitalocean_tag.email.id]

  provisioner "remote-exec" {
    connection {
      host        = self.ipv4_address
      type        = "ssh"
      private_key = file("~/.ssh/id_rsa")
    }
    inline = [
      "(echo ${random_string.droplet_user_root_password.result}; echo ${random_string.droplet_user_root_password.result}) | passwd root",
      "sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config && service ssh restart",
    ]
  }
}
data "template_file" "inventory" {
  template = file("${path.module}/templates.tmpl")
  count    = var.counts
  vars = {
    index    = count.index
    dnsnames = aws_route53_record.dns[count.index].name
  }
}

resource "local_file" "save_inventory" {
  filename = "../ansible/ansible_inventory"

  content = join("\n", data.template_file.inventory.*.rendered)
}
